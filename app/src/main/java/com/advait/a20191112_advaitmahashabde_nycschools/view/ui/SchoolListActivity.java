package com.advait.a20191112_advaitmahashabde_nycschools.view.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;

import com.advait.a20191112_advaitmahashabde_nycschools.R;
import com.advait.a20191112_advaitmahashabde_nycschools.databinding.ActivityMainBinding;
import com.advait.a20191112_advaitmahashabde_nycschools.service.model.School;
import com.advait.a20191112_advaitmahashabde_nycschools.view.adapter.SchoolAdapter;
import com.advait.a20191112_advaitmahashabde_nycschools.viewmodel.SchoolListViewModel;

import java.util.List;

/**
 * Activity which shows a list of schools.
 */
public class SchoolListActivity extends AppCompatActivity {
    private ActivityMainBinding binding;
    private SchoolAdapter schoolAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.progressBar.setVisibility(View.VISIBLE);
        binding.progressBarText.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        final SchoolListViewModel schoolListViewModel =
                ViewModelProviders.of(this).get(SchoolListViewModel.class);
        observeViewModel(schoolListViewModel);
    }

    private void setupRecyclerView() {
        RecyclerView recyclerView = binding.schoolListRecyclerview;
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        if (schoolAdapter != null) {
            recyclerView.setAdapter(schoolAdapter);
        }

    }

    private void observeViewModel(final SchoolListViewModel schoolListViewModel) {
        schoolListViewModel.getSchoolListObservable()
                .observe(this, new Observer<List<School>>() {
                    @Override
                    public void onChanged(List<School> schools) {
                        binding.progressBar.setVisibility(View.GONE);
                        binding.progressBarText.setVisibility(View.GONE);
                        if (schools != null) {
                            schoolAdapter = new SchoolAdapter(schoolListViewModel, SchoolListActivity.this);
                            setupRecyclerView();
                        }
                    }
                });
    }
}

package com.advait.a20191112_advaitmahashabde_nycschools.view.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.advait.a20191112_advaitmahashabde_nycschools.R;
import com.advait.a20191112_advaitmahashabde_nycschools.databinding.ActivitySchoolDetailBinding;
import com.advait.a20191112_advaitmahashabde_nycschools.service.model.School;
import com.advait.a20191112_advaitmahashabde_nycschools.service.model.SchoolSATData;
import com.advait.a20191112_advaitmahashabde_nycschools.viewmodel.SchoolListViewModel;

/**
 * Activity which shows the average SAT scores for a school.
 */
public class SchoolDetailActivity extends AppCompatActivity {
    private ActivitySchoolDetailBinding activitySchoolDetailBinding;

    private SchoolSATData schoolSATData;

    private SchoolListViewModel schoolListViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activitySchoolDetailBinding
                = DataBindingUtil.setContentView(this, R.layout.activity_school_detail);
        activitySchoolDetailBinding.progressBar2.setVisibility(View.VISIBLE);
        schoolSATData = getIntent().getParcelableExtra("SchoolSATData");
    }

    @Override
    protected void onResume() {
        super.onResume();
        schoolListViewModel =
                ViewModelProviders.of(this).get(SchoolListViewModel.class);
        if (schoolListViewModel.getSchoolSATDataLiveData() != null) {
            observeViewModel(schoolListViewModel);
        } else {
            setUpdatedData(schoolSATData);
        }
    }

    private void observeViewModel(final SchoolListViewModel schoolListViewModel) {
        schoolListViewModel.getSchoolSATDataLiveData()
                .observe(this, new Observer<SchoolSATData>() {
                    @Override
                    public void onChanged(SchoolSATData schoolSATData) {
                        setUpdatedData(schoolSATData);
                    }
                });
    }

    private void setUpdatedData(SchoolSATData schoolSATData) {
        activitySchoolDetailBinding.progressBar2.setVisibility(View.GONE);
        TextView criticalReadingAverageScore
                = activitySchoolDetailBinding.criticalReadingAverage;
        TextView mathAverageScore
                = activitySchoolDetailBinding.mathAverage;
        TextView writingAverageScore
                = activitySchoolDetailBinding.writingAverage;
        TextView schoolName
                = activitySchoolDetailBinding.schoolName;
        if (schoolSATData != null) {
            schoolName
                    .setText(schoolSATData.getSchoolName());
            criticalReadingAverageScore
                    .setText(String.valueOf(schoolSATData.getCriticalReadingAverageScore()));
            mathAverageScore
                    .setText(String.valueOf(schoolSATData.getMathAverageScore()));
            writingAverageScore
                    .setText(String.valueOf(schoolSATData.getWritingAverageScore()));
        }

    }
}


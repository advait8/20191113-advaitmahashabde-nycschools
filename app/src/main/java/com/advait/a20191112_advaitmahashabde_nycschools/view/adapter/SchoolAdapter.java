package com.advait.a20191112_advaitmahashabde_nycschools.view.adapter;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;

import com.advait.a20191112_advaitmahashabde_nycschools.databinding.ItemSchoolBinding;
import com.advait.a20191112_advaitmahashabde_nycschools.service.model.School;
import com.advait.a20191112_advaitmahashabde_nycschools.service.model.SchoolSATData;
import com.advait.a20191112_advaitmahashabde_nycschools.view.ui.SchoolDetailActivity;
import com.advait.a20191112_advaitmahashabde_nycschools.viewmodel.SchoolListViewModel;

import java.util.List;

/**
 * Adapter class for the list of schools
 */
public class SchoolAdapter extends RecyclerView.Adapter<SchoolAdapter.SchoolViewHolder> implements LifecycleOwner {

    private List<School> schoolList;

    private SchoolListViewModel schoolListViewModel;

    private LifecycleOwner owner;

    public SchoolAdapter(SchoolListViewModel viewModel, LifecycleOwner lifecycleOwner) {
        schoolListViewModel = viewModel;
        schoolList = schoolListViewModel.getSchoolListObservable().getValue();

        owner = lifecycleOwner;
    }

    @NonNull
    @Override
    public SchoolViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ItemSchoolBinding binding = ItemSchoolBinding.inflate(layoutInflater);
        return new SchoolViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull SchoolViewHolder holder, int position) {
        School school = schoolList.get(position);
        holder.bind(school);
    }

    @Override
    public int getItemCount() {
        return schoolList != null ? schoolList.size() : 0;
    }

    @NonNull
    @Override
    public Lifecycle getLifecycle() {
        return owner.getLifecycle();
    }

    class SchoolViewHolder extends RecyclerView.ViewHolder {
        private ItemSchoolBinding binding;

        SchoolViewHolder(final ItemSchoolBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            binding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    final Intent schoolSATDataIntent = new Intent(view.getContext(), SchoolDetailActivity.class);
                    schoolListViewModel.setSelectedSchool(binding.getSchool());
                    schoolListViewModel.fetchSchoolSATData(binding.getSchool()).observe(SchoolAdapter.this, new Observer<SchoolSATData>() {
                        @Override
                        public void onChanged(SchoolSATData schoolSATData) {
                            schoolSATDataIntent.putExtra("SchoolSATData",schoolSATData);
                            view.getContext().startActivity(schoolSATDataIntent);
                        }
                    });

                }
            });
        }

        void bind(School school) {
            binding.setSchool(school);
            binding.executePendingBindings();
        }
    }
}

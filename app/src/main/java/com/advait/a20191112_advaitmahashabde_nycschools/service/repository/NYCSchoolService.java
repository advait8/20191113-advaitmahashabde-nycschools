package com.advait.a20191112_advaitmahashabde_nycschools.service.repository;

import com.advait.a20191112_advaitmahashabde_nycschools.service.model.School;
import com.advait.a20191112_advaitmahashabde_nycschools.service.model.SchoolSATData;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * interface to hold the API calls,
 */

public interface NYCSchoolService {
    String BASE_URL
            = "https://data.cityofnewyork.us";
    /*
    API to fetch the list of high schools in NYC
     */
    @GET("/resource/s3k6-pzi2.json")
    Call<List<School>> getSchoolList();
    /*
    API to fetch the average SAT scores for a school.
     */
    @GET("/resource/f9bf-2cp4.json")
    Call<List<SchoolSATData>> getSchoolData(@Query("dbn")String dbn);
}

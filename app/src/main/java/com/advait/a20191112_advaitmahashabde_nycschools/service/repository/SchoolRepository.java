package com.advait.a20191112_advaitmahashabde_nycschools.service.repository;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.advait.a20191112_advaitmahashabde_nycschools.service.model.School;
import com.advait.a20191112_advaitmahashabde_nycschools.service.model.SchoolSATData;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Repository to hold all interactions with API.
 */
public class SchoolRepository {
    private static NYCSchoolService schoolService;

    public static SchoolRepository getInstance(NYCSchoolService service) {
        schoolService = service;
        return new SchoolRepository();
    }

    public LiveData<List<School>> getSchoolList() {
        final MutableLiveData<List<School>> data = new MutableLiveData<>();
        schoolService.getSchoolList().enqueue(new Callback<List<School>>() {
            @Override
            public void onResponse(Call<List<School>> call, Response<List<School>> response) {
                data.setValue(response.body());
            }

            @Override
            public void onFailure(Call<List<School>> call, Throwable t) {
                Log.d("NYCSchools", "API Call failed");
            }
        });
        return data;
    }

    public LiveData<SchoolSATData> getSchoolData(String schoolId) {
        final MutableLiveData<SchoolSATData> data = new MutableLiveData<>();
        schoolService.getSchoolData(schoolId).enqueue(new Callback<List<SchoolSATData>>() {
            List dataArrayList = new ArrayList();

            @Override
            public void onResponse(Call<List<SchoolSATData>> call, Response<List<SchoolSATData>> response) {
                dataArrayList = response.body();
                if (dataArrayList != null && !dataArrayList.isEmpty()) {
                    data.setValue((SchoolSATData) dataArrayList.get(0));
                }
            }

            @Override
            public void onFailure(Call<List<SchoolSATData>> call, Throwable t) {
                Log.d("NYCSchools", t.getLocalizedMessage());
            }
        });
        return data;
    }
}

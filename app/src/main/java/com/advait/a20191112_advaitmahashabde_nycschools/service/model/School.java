package com.advait.a20191112_advaitmahashabde_nycschools.service.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * model class to hold school details
 */
public class School implements Parcelable {
    @SerializedName(value = "dbn")
    private String schoolId;

    @SerializedName(value="school_name")
    private String schoolName;

    private String website;

    protected School(Parcel in) {
        schoolId = in.readString();
        schoolName = in.readString();
        website = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(schoolId);
        dest.writeString(schoolName);
        dest.writeString(website);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<School> CREATOR = new Creator<School>() {
        @Override
        public School createFromParcel(Parcel in) {
            return new School(in);
        }

        @Override
        public School[] newArray(int size) {
            return new School[size];
        }
    };

    public String getSchoolId() {
        return schoolId;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public String getWebsite() {
        return website;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public void setWebsite(String website) {
        this.website = website;
    }
}

package com.advait.a20191112_advaitmahashabde_nycschools.service.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * model class to hold school's average SAT scores
 */
public class SchoolSATData implements Parcelable {
    @SerializedName(value="schoolId")
    private String schoolId;

    @SerializedName(value="school_name")
    private String schoolName;

    @SerializedName(value="sat_critical_reading_avg_score")
    private int criticalReadingAverageScore;

    @SerializedName(value="sat_math_avg_score")
    private int mathAverageScore;

    @SerializedName(value="sat_writing_avg_score")
    private int writingAverageScore;

    protected SchoolSATData(Parcel in) {
        schoolId = in.readString();
        schoolName = in.readString();
        criticalReadingAverageScore = in.readInt();
        mathAverageScore = in.readInt();
        writingAverageScore = in.readInt();
    }

    public static final Creator<SchoolSATData> CREATOR = new Creator<SchoolSATData>() {
        @Override
        public SchoolSATData createFromParcel(Parcel in) {
            return new SchoolSATData(in);
        }

        @Override
        public SchoolSATData[] newArray(int size) {
            return new SchoolSATData[size];
        }
    };

    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public int getCriticalReadingAverageScore() {
        return criticalReadingAverageScore;
    }

    public void setCriticalReadingAverageScore(int criticalReadingAverageScore) {
        this.criticalReadingAverageScore = criticalReadingAverageScore;
    }

    public int getMathAverageScore() {
        return mathAverageScore;
    }

    public void setMathAverageScore(int mathAverageScore) {
        this.mathAverageScore = mathAverageScore;
    }

    public int getWritingAverageScore() {
        return writingAverageScore;
    }

    public void setWritingAverageScore(int writingAverageScore) {
        this.writingAverageScore = writingAverageScore;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(schoolId);
        parcel.writeString(schoolName);
        parcel.writeInt(criticalReadingAverageScore);
        parcel.writeInt(mathAverageScore);
        parcel.writeInt(writingAverageScore);
    }
}

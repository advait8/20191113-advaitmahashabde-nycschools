package com.advait.a20191112_advaitmahashabde_nycschools.service.repository;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.advait.a20191112_advaitmahashabde_nycschools.service.repository.NYCSchoolService.BASE_URL;
/*
Singleton class to get instance of retrofit object
 */
public class RetrofitClientInstance {
    private static Retrofit retrofit;
    public static Retrofit getRetrofitInstance() {
        if (retrofit == null) {
            retrofit = new retrofit2.Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}

package com.advait.a20191112_advaitmahashabde_nycschools.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.advait.a20191112_advaitmahashabde_nycschools.service.model.School;
import com.advait.a20191112_advaitmahashabde_nycschools.service.model.SchoolSATData;
import com.advait.a20191112_advaitmahashabde_nycschools.service.repository.NYCSchoolService;
import com.advait.a20191112_advaitmahashabde_nycschools.service.repository.RetrofitClientInstance;
import com.advait.a20191112_advaitmahashabde_nycschools.service.repository.SchoolRepository;

import java.util.List;

/**
 * ViewModel for list of schools.
 */
public class SchoolListViewModel extends AndroidViewModel {
    private final SchoolRepository schoolRepository;
    private final NYCSchoolService schoolService;
    private final LiveData<List<School>> schoolListObservable;
    private LiveData<SchoolSATData> schoolSATDataLiveData;
    private MutableLiveData<School> selectedSchool = new MutableLiveData<>();

    public SchoolListViewModel(@NonNull Application application) {
        super(application);
        schoolService = RetrofitClientInstance
                .getRetrofitInstance().create(NYCSchoolService.class);
        schoolRepository = SchoolRepository.getInstance(schoolService);
        schoolListObservable = schoolRepository.getSchoolList();
    }

    public LiveData<List<School>> getSchoolListObservable() {
        return schoolListObservable;
    }

    public LiveData<SchoolSATData> fetchSchoolSATData(School school) {
        schoolSATDataLiveData =
                schoolRepository.getSchoolData(school.getSchoolId());
        return schoolSATDataLiveData;
    }

    public void setSelectedSchool(School selectedSchool) {
        this.selectedSchool.setValue(selectedSchool);
    }

    public MutableLiveData<School> getSelectedSchool() {
        return selectedSchool;
    }

    public LiveData<SchoolSATData> getSchoolSATDataLiveData() {
        return schoolSATDataLiveData;
    }
}

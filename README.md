Android app to fetch list of high schools in NYC and on tapping any school, show their SAT scores.

* Built with MVVM architecture
* Retrofit to make API calls. 
* UI logic in views 
* business logic + API calls in viewModel.
* School and School SAT Scores represented as models. 
